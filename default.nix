with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "pandoc-test";
  src = ./.;

  buildInputs = [
    gnumake
    pandoc
    python3Packages.httpserver
  ];

  buildPhase = ''
    BASEURL="/pandoc-test" make
  '';

  installPhase = ''
    mkdir -p $out/public
    cp -R site/* $out/public/
    find $out/public \( -name '*.html' -o -name '*.css' -o -name '*.js' -o -name '*.svg' \) -print0 | xargs -0 gzip -9 -k -f
  '';

}

