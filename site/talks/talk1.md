---
title: My talk template
date: 2021-08-19
author: Author One, Author Two
---


# Section 1 

## Lists

- foo
- bar
- foobar
- barfoo

## Code

```nix
with import <nixpkgs> {};
stdenv.mkDerivation rec {
  name = "pandoc-test";
  src = ./.;
  buildInputs = [
    gnumake
    pandoc
  ];
  installPhase = ''
    mkdir -p $out
    TODO
  '';
}
```

```python
if __name__ == "__main__":
    print("Hello world")
```

# Section 2 

## Maths

$$F^{42}(n) = 
\left\{
    \begin{array}{l}
        n \text{ si } n \in \{0, 1\} \\
        F^{42}(n-1) + F^{42}(n-2) \mod 42 \text{ sinon }
    \end{array}
    \right.$$

## Images

![](spongebob.png){width="20%"}


# Conclusion


## References

- <https://nixos.org>

