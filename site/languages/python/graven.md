---
title: Critique du cours vidéo sur python de Graven
date: 2021-08-19
---

Le cours intitulé "Apprendre le Python" est un cours proposé sur la chaîne youtube Gravenilvectuto. Le cours se présente comme un cours pour apprendre les fondamentaux du langage de programmation Python. Il est constitué de 9 vidéos sur le langage d'une moyenne de 13 minutes et d'une vidéo sur la bibliothèque Tkinter de 40 minutes.

# Les défauts du cours

## Épisode 1 - Introduction

Contrairement à ce qui est présenté dans la vidéo, un IDE (environnement de développement intégré) n'est pas *nécessaire* pour développer. Il est d'ailleurs plus commun de croiser des développeurs python qui utilisent le combo éditeur de texte et terminal du fait de la légèreté du langage. [J'ai écrit un billet à ce sujet](https://docs.drlazor.be/python_ide.md).

L'ensemble des vidéos sera capturé dans cet environnement. On peut regretter que le vidéaste ne désactive pas les différents linters et correcteurs orthographiques. Les démonstrations sont sans-cesse interrompues par myriade de pop-up de conseil et les différents mots français sont régulièrement soulignés inutilement par le correcteur orthographique.

Lors de l'installation de python, l'attention du téléspectateur n'est pas attirée sur l'option "Add python to the PATH", il s'agit d'une case à cocher qui rend accessible les programmes `python`, `py` et `pip` via la ligne de commande. Les débutants ne savent généralement pas comment changer les variables d'environnement sur Windows et se plaignent de ne pas réussir à lancer python car `'python' n’est pas reconnu en tant que commande interne ou externe, un programme exécutable ou un fichier de commandes.`.

Le tout premier snippet de code montré à l'écran est le suivant:

```python
if __name__ == "__main__":
    print("Hello world")
```

Si il est commun de voir un `if __name__ == "__main__"` dans les projets Python, il n'est pas nécessaire et ne présente un intérêt qu'au moment où les modules sont introduits. Il aurait été plus judicieux de ne pas l'introduire pour ne pas rajouter de la complexité inutile.

Le dernier point sera heureusement rectifié dans deux vidéos.

## Cours de Java... en Python...

Si la qualité visuelle des vidéos et la dynamique du cours sont supérieures à bon nombres de cours en lignes (gratuit et payant), le côté technique est quant à lui approximatif. L'approche pédagogique de l'auteur souffre de nombreux biais qui sont communs aux développeurs Java qui s'essaient au langage Python.

Plusieurs arguments penchent en cette faveur :

* Il est commun d'utiliser un IDE complet pour développer en Java, pas en Python ;
* Il y a conversion automatique des entiers vers les chaînes de caractère en Java, en Python elles doivent être explicites ;
* L'expression `condition ? true_value : false_value` est une expression ternaire en Java, l'expression python `(false_value:true_value)[condition]` y ressemble fâcheusement (expressions finales d'un côté, condition de l'autre) mais n'est pas une expression ternaire en Python ;
* Le *for* classique n'existe pas en Python, il est simulé par l'objet range mais il n'existe en Python bien que ce que les développeurs Java appellent le *for each* ;
* Les bonnes pratiques Java dictent qu'il est important de définir des méthodes dédiées (*getters* et *setters*) pour manipuler les attributs d'un objet, les bonnes pratiques Python dictent de modifier directement l'attribut ou de passer par des propriétés.


