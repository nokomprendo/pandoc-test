---
title: This is my Pandoc Test
---

bloublou

[source code](https://gitlab.com/nokomprendo/pandoc-test)

## Talks

- [talk 1](talks/talk1.html)

## Languages

### Haskell

- [hello](languages/haskell/hello.html)

### Python

- [Critique du cours vidéo sur python de Graven](languages/python/graven.html)

