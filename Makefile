MD = $(shell find site -name "*.md")
HTML = $(MD:.md=.html)

all: $(HTML)

%.html: %.md
	pandoc $< -o $@ --template template.html --katex -V baseurl="$(BASEURL)"

run: $(HTML)
	python3 -m http.server -d site

clean:
	rm -f $(HTML)

